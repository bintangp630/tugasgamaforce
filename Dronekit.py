from dronekit import connect, VehicleMode, LocationGlobalRelative
import time as ti
import argparse as a

#dronekit-sitl copter --home=-7.771770, 110.376578, 0, 180



#connect to vehicle
parser = a.ArgumentParser(description='commands')
parser.add_argument('--connect')
args = parser.parse_args()

connection_string = args.connect

print("Connection to the vehicle on %s"%connection_string)
vehicle = connect(connection_string, wait_ready=True)

#takeoff
def arm_and_takeoff(tgt_altitude):
    print ("Arming motors")
    while not vehicle.is_armable:
        ti.sleep(1)

    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True

    print("TakeOff")
    vehicle.simple_takeoff(tgt_altitude)

    #target altitude
    while True:
        altitude = vehicle.location.global_relative_frame.alt

        if altitude >= tgt_altitude -1:
            print("Altitude reached")
            break

        ti.sleep(1)

#main
arm_and_takeoff(10)

#speed
vehicle.airspeed = 9

#wpl
print ("jalan-jalan")
wpl1 = LocationGlobalRelative(-7.769532, 110.377311)
wpl2 = LocationGlobalRelative(-7.769918, 110.378704)
wpl3 = LocationGlobalRelative(-7.772222, 110.377999)

vehicle.simple_goto(wpl1)
vehicle.simple_goto(wpl2)
vehicle.simple_goto(wpl3)

ti.sleep(30)

#balik
print("Balik")
vehicle.mode = VehicleMode("RTL")

ti.sleep(20)

#close
vehicle.close()
